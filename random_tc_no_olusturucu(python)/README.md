# Nasıl çalışıyor?
*Python'daki random kütüphanesiyle T.C. kimlik nosunun ilk 9 rakamı rastgele oluşturulduktan sonra [T.C. kimlik no algoritmasına](https://www.webtekno.com/t-c-kimlik-numarasi-nasil-belirlenir-h122766.html) göre 10. rakamını ilk 9 rakamıyla, 11. rakamını ise ilk 10 rakamıyla hesaplayarak T.C. kimlik numaralarını oluşturmaktadır.*


![](https://gitlab.com/C4F3RS/tc_kimlik_no_programlari/-/raw/main/resimler/1652100892725.jpg)
